# mkiso

Скрипт для создания LiveCD/USB образа дистрибутива. Форк скрипта из Olean Linux.

Использование:

```bash
sudo bash mkiso.sh [путь до директории с дистрибутивом] [название iso-образа]
```

Например:

```bash
sudo bash mkiso.sh /mnt/lin calmira-lx4-1.1.iso
```
